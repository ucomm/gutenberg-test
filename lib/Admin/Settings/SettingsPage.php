<?php

namespace Boilerplate\Admin\Settings;

class SettingsPage {
  private $menuSlug;

  public function __construct() {
    $this->menuSlug = 'boilerplate-settings-menu';
  }

  public function init() {
    add_action('admin_menu', [$this, 'addMenuPage']);

    add_action('admin_menu', [$this, 'addSubmenuPage']);
  }

  public function addMenuPage() {
    add_menu_page(
      __('Boilerplate Settings', 'boilerplate'),
      __('Boilerplate Settings', 'boilerplate'),
      'manage_options',
      $this->menuSlug,
      [$this, 'getParentSettingsPage'],
      'dashicons-groups'
    );
  }

  public function addSubmenuPage() {
    $slug = $this->menuSlug . 'submenu-settings';

    add_submenu_page(
      $this->menuSlug,
      __('Submenu Settings', 'uconn-admission-utils'),
      __('Submenu Settings', 'uconn-admission-utils'),
      'manage_options',
      $slug,
      [ $this, 'getSubmenuSettingsPage' ]
    );
  }

  public function getParentSettingsPage() {
    include PLUGIN_DIR . 'partials/admin/parent-settings-page.php';
  }

  public function getSubmenuSettingsPage() {
    include PLUGIN_DIR . 'partials/admin/submenu-settings-page.php';
  }

}