<?php

namespace Boilerplate\Endpoints;

class Example extends Controller
{
  public function registerRoutes() {
    register_rest_route($this->namespace, '/' . $this->resource, [
      [
        'methods' => 'GET',
        'callback' => [$this, 'getResponse']
      ]
    ]);
  }

  public function getResponse() {
    $data = [
      'message' => 'hello world',
    ];

    return rest_ensure_response($data);
  }
}
