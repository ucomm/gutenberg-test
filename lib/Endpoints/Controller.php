<?php

namespace Boilerplate\Endpoints;

use Boilerplate\Dotenv\Dotenv;
use WP_REST_Controller;

/**
 * This class helps manage REST API endpoints. The endpoint namespace and resource are centrally managed from here which makes updates easier. All classes that inherit from here will need to implement the registerRoutes method.
 * 
 * If you need it, you can easily access the database using the Database class. 
 * 
 * You can also use this as a model for extending WP_REST_Posts_Controller
 */
abstract class Controller extends WP_REST_Controller {
  protected $namespace;
  protected $resource;

  public function __construct(string $namespace = '/uconn/v1', string $resource = 'resources') {
    $this->namespace = $namespace;
    $this->resource = $resource;

    $dotenv = new Dotenv();
    $dotenv->createEnvGlobals();
  }

  /**
   * Register the routes for the API endpoints
   *
   * @return void
   */
  public function apiInit() {
    add_action('rest_api_init', [$this, 'registerRoutes']);
  }

  /**
   * Checks basic authentication of the requests to an endpoint
   *
   * @param string $user
   * @param string $pass
   * @return boolean
   */
  protected function authenticateRequest(string $user, string $pass): bool {
    $authenticated = false;

    $headers = getallheaders();

    $auth = explode(' ', $headers['Authorization']);
    $authArray = explode(':', base64_decode($auth[1]));

    if ($authArray[0] === $user && $authArray[1] === $pass) {
      $authenticated = true;
    }

    return $authenticated;
  }

  /**
   * Sets an authentication message that can be returned as part of a response.
   *
   * @param boolean $isAuthenticated
   * @return array
   */
  protected function setAuthenticationMessage(bool $isAuthenticated): string {
    return !$isAuthenticated ?
      'Unauthorized Request: Invalid username or password' :
      'Authenticated';
  }

  /**
   * Enumerate the routes to be registered in the class
   *
   * @return void
   */
  abstract function registerRoutes();
}