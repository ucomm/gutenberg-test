<?php

namespace Boilerplate\Frontend;

class Shortcode {
  public static $shortcodeSlug = 'boilerplate-shortcode';

  public function addShortcode() {
    add_shortcode($this->shortcodeSlug, [ $this, 'display' ]);
  }

  public function display() {
    ob_start();
    include(PLUGIN_DIR . '/partials/public/public-display.php');
    return ob_get_clean();
  }

  public static function getShortcodeSlug(): string {
    return self::$shortcodeSlug;
  }
}