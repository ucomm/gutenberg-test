<?php

namespace Boilerplate\Assets;

use UCTC\Frontend\Shortcode;

abstract class Loader {
  protected $handle;
  protected $adminHandle;
  protected $buildDir;
  protected $shortcodeSlug;

  public function __construct() {
    $this->handle = $this->setHandle();
    $this->adminHandle = $this->setAdminHandle();
    // set the build directory based on environment
    $this->buildDir = $_SERVER['HTTP_HOST'] === 'localhost' ?
      'dev-build' :
      'build';
    $this->shortcodeSlug = Shortcode::getShortcodeSlug();
  }

  /**
   * Run the wp_enqueue_scripts action
   *
   * @return void
   */
  public function enqueueAssets() {
    add_action('wp_enqueue_scripts', [ $this, 'enqueue' ]);
  }

  /**
   * Run the admin_enqueue_scripts action
   *
   * @return void
   */
  public function enqueueAdminAssets() {
    add_action('admin_enqueue_scripts', [$this, 'adminEnqueue']);
  }

  /**
   * Enqueue assets when the wp_enqueue_scripts action is called
   *
   * @return void
   */
  abstract function enqueue();

  /**
   * Enqueue assets when the admin_enqueue_scripts action is called
   *
   * @return void
   */
  abstract function adminEnqueue(string $hook);

  /**
   * Set the base handle for scripts and styles
   *
   * @return string
   */
  protected function setHandle(): string {
    return 'boilerplate';
  }

  /**
   * Set the base handle for admin scripts and styles
   *
   * @return string
   */
  protected function setAdminHandle(): string {
    return 'boilerplate-admin';
  }
}