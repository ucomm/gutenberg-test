<?php
$width = $block_attributes['testDimensions']['width'];
$height = $block_attributes['testDimensions']['height'];
?>

<div class="random-image">
  <p>Hello random image</p>
  <img src="https://source.unsplash.com/<?php echo $height; ?>x<?php echo $width; ?>/?<?php echo $block_attributes['keywords']; ?>" alt="">
</div>