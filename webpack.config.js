const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const DependencyExtractionPlugin = require('@wordpress/dependency-extraction-webpack-plugin')

const prodEnv = process.env.NODE_ENV === 'prodution' ? true : false

let mode = 'development'
let buildDir = path.resolve(__dirname, 'dev-build')

if (prodEnv) {
  mode = 'production'
  buildDir = path.resolve(__dirname, 'build')
}

module.exports = {
  entry: {
    main: path.resolve(__dirname, 'src', 'index.js'),
    admin: path.resolve(__dirname, 'src', 'admin.js'),
    accordion: path.resolve(__dirname, 'src', 'accordion.js'),
    reactForm: path.resolve(__dirname, 'src', 'formBlock.js')
  },
  output: {
    path: buildDir,
    filename: '[name].js',
    chunkFilename: '[name].js'
  },
  mode,
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              // plugins: [ 'lodash' ],
              presets: [
                [
                  '@babel/preset-env',
                  {
                    debug: true,
                    useBuiltIns: 'usage',
                    corejs: 3.13
                  }
                ],
                '@babel/preset-react'
              ]
            }
          }
        ]
      },
      {
        test: /\.s?css$/i,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { sourceMap: true } },
          { loader: 'sass-loader', options: { sourceMap: true } }
        ]
      }
    ]
  },
  resolve: {
    extensions: [ '.js', '.jsx', '.css', '.scss' ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
    // create *.asset.php files
    new DependencyExtractionPlugin({
      useDefaults: true
    })
  ],
  // externals: {
  //   lodash: 'lodash'
  // }
}