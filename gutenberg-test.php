<?php
/*
Plugin Name: Gutenberg Test Plugin
Description: Minimal WP plugin boilerplate
Author: UComm Web Team
Version: 0.0.1
Text Domain: gutenberg-test
*/

// rename this file according to the plugin.

// use Boilerplate\Assets\ScriptLoader;
// use Boilerplate\Assets\StyleLoader;
// use Boilerplate\Endpoints\Example;
// use Boilerplate\Frontend\Shortcode;

if (!defined('WPINC')) {
	die;
}

define( 'PLUGIN_DIR', plugin_dir_path(__FILE__) );
define( 'PLUGIN_URL', plugins_url('/', __FILE__) );

function register_blocks() {

	$adminAssetFile = include(PLUGIN_DIR . 'dev-build/admin.asset.php');
	$mainAssetFile = include(PLUGIN_DIR . 'dev-build/main.asset.php');
	$accordionAssetFile = include(PLUGIN_DIR . 'dev-build/accordion.asset.php');
	$reactFormAssetFile = include(PLUGIN_DIR . 'dev-build/reactForm.asset.php');

	$reactDep = [ 'wp-element' ];
	$formDeps = array_merge($reactFormAssetFile['dependencies'], $reactDep);

	wp_register_script(
		'demo-block-1-admin-script',
		PLUGIN_URL . 'dev-build/admin.js',
		$adminAssetFile['dependencies'],
		$adminAssetFile['version'],
		true
	);

	wp_register_style(
		'demo-block-1-admin-style',
		PLUGIN_URL . 'dev-build/admin.css',
		['wp-edit-blocks'],
		filemtime(PLUGIN_DIR . 'dev-build/admin.css')
	);

	wp_register_script(
		'demo-block-1-script',
		PLUGIN_URL . 'dev-build/main.js',
		$mainAssetFile['dependencies'],
		$mainAssetFile['version'],
		true
	);

	wp_register_style(
		'demo-block-1-style',
		PLUGIN_URL . 'dev-build/main.css',
		[],
		filemtime(PLUGIN_DIR . 'dev-build/main.css')
	);

	wp_register_script(
		'demo-accordion-script',
		PLUGIN_URL . 'dev-build/accordion.js',
		$accordionAssetFile['dependencies'],
		$accordionAssetFile['version'],
		true
	);

	wp_register_script(
		'demo-react-form-script',
		PLUGIN_URL . 'dev-build/reactForm.js',
		$formDeps,
		$reactFormAssetFile['version'],
		true
	);

	// https://developer.wordpress.org/block-editor/reference-guides/block-api/block-metadata/
	// the array argument can be any valid argument as in register_block_type
	register_block_type_from_metadata(PLUGIN_DIR . 'block-configs/text-block.json', [
		'style' => 'demo-block-1-style',
		'editor_script' => 'demo-block-1-admin-script',
		'editor_style' => 'demo-block-1-admin-style'
	]);

	register_block_type_from_metadata(PLUGIN_DIR . 'block-configs/profile-block.json', [
		'style' => 'demo-block-1-style',
		'editor_script' => 'demo-block-1-admin-script',
		'editor_style' => 'demo-block-1-admin-style'
	]);

	register_block_type_from_metadata(PLUGIN_DIR . 'block-configs/random-image-block.json', [
		'style' => 'demo-block-1-style',
		'editor_script' => 'demo-block-1-admin-script',
		'editor_style' => 'demo-block-1-admin-style',
		'render_callback' => function($block_attributes, $content) {
			// echo "<pre>";
			// var_dump($block_attributes);
			// echo "</pre>";
			// echo "<pre>";
			// var_dump($content);
			// echo "</pre>";

			ob_start();

			include(PLUGIN_DIR . 'partials/public/random-image.php');

			return ob_get_clean();
		}
	]);
	
	register_block_type_from_metadata(PLUGIN_DIR . 'block-configs/blockquote-block.json', [
		'style' => 'demo-block-1-style',
		'editor_script' => 'demo-block-1-admin-script',
		'editor_style' => 'demo-block-1-admin-style'
	]);


	register_block_type_from_metadata(PLUGIN_DIR . 'block-configs/accordion-block.json', [
		'style' => 'demo-block-1-style',
		'script' => 'demo-accordion-script',
		'editor_script' => 'demo-block-1-admin-script',
		'editor_style' => 'demo-block-1-admin-style'
	]);

	register_block_type_from_metadata(PLUGIN_DIR . 'block-configs/accordion-item-block.json', [
		'style' => 'demo-block-1-style',
		'editor_script' => 'demo-block-1-admin-script',
		'editor_style' => 'demo-block-1-admin-style'
	]);

	register_block_type_from_metadata(PLUGIN_DIR . 'block-configs/react-form-block.json', [
		'style' => 'demo-block-1-style',
		'script' => 'demo-react-form-script',
		'editor_script' => 'demo-block-1-admin-script',
		'editor_style' => 'demo-block-1-admin-style'
	]);



	/**
	 * 
	 * You can register a block like this. However, it's probably better to do it off one file as shown above.
	 * 
	 */
	// register_block_type('demo-blocks/first-demo-block', [
	// 	'api_version' => 2,
	// 	'title' => 'First Demo Block',
	// 	'icon' => 'universal-access-alt',
	// 	'category' => 'text',
	// 	// 'description' => 'Going deeper into gutenberg block development',
	// 	// 'script' => 'demo-block-1-script',
	// 	'style' => 'demo-block-1-style',
	// 	'editor_script' => 'demo-block-1-admin-script',
	// 	'editor_style' => 'demo-block-1-admin-style'
	// ]);

	// echo "<pre>";
	// var_dump($registered);
	// echo "</pre>";
}

add_action('init', 'register_blocks');

function filterBlockCategories($categories) {
	return array_merge(
		[
			[
				'slug' => 'uconn-blocks',
				'title' => __('UConn Blocks', 'gutenberg-test'),
				'icon' => 'wordpress'
			]
		],
		$categories
	);
}

add_filter('block_categories', 'filterBlockCategories', 10, 2);

// select the right composer autoload.php file depending on environment.
// if (file_exists(dirname(ABSPATH) . '/vendor/autoload.php')) {
// 	require_once(dirname(ABSPATH) . '/vendor/autoload.php');
// } elseif (file_exists(ABSPATH . 'vendor/autoload.php')) {
// 	require_once(ABSPATH . 'vendor/autoload.php');
// } else {
// 	require_once('vendor/autoload.php');
// }

// require('lib/Admin/Settings/SettingsPage.php');
// require('lib/Admin/Settings/Options.php');
// require('lib/Assets/Loader.php');
// require('lib/Assets/ScriptLoader.php');
// require('lib/Assets/StyleLoader.php');
// require('lib/Database/Database.php');
// require('lib/Dotenv/Dotenv.php');
// require('lib/Endpoints/Controller.php');
// require('lib/Endpoints/Example.php');
// require('lib/Files/FileManager.php');
// require('lib/Frontend/Shortcode.php');

// $endpoints = [
// 	'example' => new Example()
// ];

// foreach ($endpoints as $endpoint) {
// 	$endpoint->apiInit();
// }

// $sciptLoader = new ScriptLoader();
// $styleLoader = new StyleLoader();

// if (!is_admin()) {
// 	$sciptLoader->enqueueAssets();
// 	$styleLoader->enqueueAssets();
// } else {
// 	$sciptLoader->enqueueAdminAssets();
// 	$styleLoader->enqueueAdminAssets();
// }

// $shortcode = new Shortcode();
// $shortcode->addShortcode();