import { useState, useEffect } from '@wordpress/element'
import useCount from '../utils/useCount'
import TextInput from './textInput'

const Form = ({ word, isEdit = true }) => {

  const [ count, setCount ] = useCount()

  const [ name, setName ] = useState('')

  const formSubmission = async (data = {}) => {
    const res = await fetch(
      'http://localhost',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
    )
    // console.log({ res });
    return res.json()
    // console.log({ json });
  }


  const submitHandler = (evt) => {

    evt.preventDefault()

    if (isEdit) return

    console.log({ evt, name })

    formSubmission({ name })
      // .then(data => console.log(data))
      // .catch(err => console.log({ err }))
    // console.log();

    // .then(({ status, body }) => {
    //   const data = body.json()
    //   console.log({ data });
    // }).catch(err => console.log({ err }))
  }

  return (
    <div>
      {word}
      <button onClick={setCount}>
        Click
      </button>
      <p>Count - {count}</p>
      <p>Name - { name }</p>
      <form onSubmit={submitHandler}>
        <TextInput 
          id="name"
          onChangeHandler={setName}
        />
        <input type="submit" value="Send" />
      </form>
    </div>
  )
}

export default Form