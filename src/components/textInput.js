const TextInput = ({ id, onChangeHandler }) => {
  return (
    <div>
      <label htmlFor={id}>Name:
        <input 
          type="text" 
          id={id} 
          onChange={({ target }) => onChangeHandler(target.value)}
        />
      </label>
    </div>
  )
}

export default TextInput