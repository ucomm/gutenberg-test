import { render } from '@wordpress/element'

import Form from './components/form'

const formBlocks = document.querySelectorAll('.wp-block-demo-blocks-react-form')

const MyFormWrapper = () => {

  return (
    <div>
      <Form 
        word="front"
        isEdit={false}
      />
    </div>
  )
}

formBlocks.forEach(block => render(<MyFormWrapper />, block));

