import { registerBlockType } from '@wordpress/blocks'
import { textBlockData, textBlockName } from './demo-blocks/text-block'
import { profileBlockName, profileBlockData } from './demo-blocks/profile-block'
import { randomImageBlockName, randomImageBlockData } from './demo-blocks/random-image-block'
import { blockQuoteName, blockQuoteData } from './demo-blocks/blockquote-block'
import { accordionBlockData, accordionBlockName } from './demo-blocks/accordion-block'
import { accordionItemData, accordionItemName } from './demo-blocks/accordion-item-block'
import { reactFormName, reactFormData } from './demo-blocks/react-form-block'

import './sass/admin/admin.scss'



registerBlockType(textBlockName, textBlockData)
registerBlockType(profileBlockName, profileBlockData)
registerBlockType(randomImageBlockName, randomImageBlockData)
registerBlockType(blockQuoteName, blockQuoteData)
registerBlockType(accordionBlockName, accordionBlockData)
registerBlockType(accordionItemName, accordionItemData)
registerBlockType(reactFormName, reactFormData)