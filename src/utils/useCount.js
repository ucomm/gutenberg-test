import { useState, useCallback } from 'react'

function useCount(initialCount = 0) {

  // const [ count, setCount ] = useState(initialCount)
  const [ state, setState ] = useState(initialCount)

  const addToCount = useCallback(() => setState((state) => state + 1))

  return [ state, addToCount ]

  // return () => {
  //   setCount(count + 1)
  //   console.log({ count });
  //   return count
  // }
}

export default useCount