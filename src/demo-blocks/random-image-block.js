import {
  useBlockProps,
  // BlockControls,
  __experimentalImageSizeControl as ImageSizeControl,
  InspectorControls,
} from '@wordpress/block-editor'
// import ImageSizeControl from '@wordpress/block-editor'
import { 
  TextControl, 
  // SelectControl,
  PanelBody
} from '@wordpress/components'

const randomImageBlockJSON = require('../../block-configs/random-image-block.json')

export const randomImageBlockName = randomImageBlockJSON.name

export const randomImageBlockData = {
  apiVersion: randomImageBlockJSON.apiVersion,
  title: randomImageBlockJSON.title,
  icon: randomImageBlockJSON.icon,
  category: randomImageBlockJSON.category,
  attributes: randomImageBlockJSON.attributes,
  edit: (props) => {
    const { attributes, setAttributes } = props
    return (
      <div {...useBlockProps()}>
        <TextControl
          label="Keywords"
          value={attributes.keywords}
          onChange={(newKeywords) => setAttributes({ keywords: newKeywords })}
        />
        <InspectorControls>
          <PanelBody title="Image Settings">
            <ImageSizeControl 
              onChange={(value) => {

                const { testDimensions } = attributes

                const hasHeight = value.hasOwnProperty('height')
                const hasWidth = value.hasOwnProperty('width')

                // make sure that the dimensions always fall back to the attributes if there isn't a new value
                setAttributes({ testDimensions: {
                    height: hasHeight ? value.height : testDimensions.height,
                    width: hasWidth ? value.width : testDimensions.width
                  } 
                })

              }}
              width={attributes.testDimensions.width}
              height={attributes.testDimensions.height}
              imageWidth={attributes.testDimensions.width}
              imageHeight={attributes.testDimensions.height}
            />
          </PanelBody>
        </InspectorControls>
        <div>
          <img src={`https://source.unsplash.com/${attributes.testDimensions.height}x${attributes.testDimensions.width}/?${attributes.keywords}`} alt="" />
        </div>
      </div>
    )
  }
}