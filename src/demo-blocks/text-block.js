import {
  useBlockProps,
  RichText,
  AlignmentToolbar,
  BlockControls,
  ColorPalette,
  InspectorControls
} from '@wordpress/block-editor'

// use a json file to keep attributes for blocks consistent
const textBlockJson = require('../../block-configs/text-block.json')

export const textBlockName = textBlockJson.name

export const textBlockData = {
  apiVersion: textBlockJson.apiVersion,
  title: textBlockJson.title,
  icon: textBlockJson.icon,
  category: textBlockJson.category,
  attributes: {
    alignment: {
      type: 'string',
      default: 'none'
    },
    content: {
      type: 'array',
      source: 'children',
      selector: 'p'
    },
    bgColor: {
      type: 'string',
      default: '#000000'
    },
    textColor: {
      type: 'string',
      default: '#ffffff'
    }
  },
  example: {
    attributes: {
      content: 'Hello world',
      alignment: 'right',
      bgColor: '#000000',
      textColor: '#ffffff'
    }
  },
  edit: (props) => {
    const {
      attributes: {
        alignment,
        bgColor,
        content,
        textColor
      },
      setAttributes,
      className
    } = props

    const blockProps = useBlockProps({
      className: 'test-block'
    })

    const onChangeContent = (newContent) => {
      setAttributes({ content: newContent })
    }

    const onChangeAlignment = (newAlignment) => {
      setAttributes({
        alignment: newAlignment === undefined ? 'none' : newAlignment
      })
    }

    const onChangeBGColor = (hexColor) => {
      setAttributes({ bgColor: hexColor })
    }

    const onChangeTextColor = (hexColor) => {
      setAttributes({ textColor: hexColor })
    }

    return (
      <div {...blockProps}>
        <InspectorControls key="setting">
          <div>
            <fieldset>
              <legend>Background color</legend>
              <ColorPalette onChange={onChangeBGColor} />
            </fieldset>
            <fieldset>
              <legend>Text color</legend>
              <ColorPalette onChange={onChangeTextColor} />
            </fieldset>
          </div>
        </InspectorControls>
        <BlockControls>
          <AlignmentToolbar
            value={alignment}
            onChange={onChangeAlignment}
          />
        </BlockControls>
        <RichText
          style={{
            backgroundColor: bgColor,
            borderColor: bgColor,
            color: textColor,
            textAlign: alignment,
          }}
          tagName="p"
          onChange={onChangeContent}
          value={content}
        />
      </div>
    )
  },
  save: ({ attributes }) => {
    const blockProps = useBlockProps.save()
    return (
      <div {...blockProps}>
        <RichText.Content
          tagName="p"
          value={attributes.content}
          className={`demo-block-align-${attributes.alignment}`}
          style={{
            backgroundColor: attributes.bgColor,
            borderColor: attributes.bgColor,
            color: attributes.textColor
          }}
        />
      </div>
    )
  }
}