import { InnerBlocks, useBlockProps } from '@wordpress/block-editor'

const profileBlockJSON = require('../../block-configs/profile-block.json')

export const profileBlockName = profileBlockJSON.name

export const profileBlockData = {
  apiVersion: profileBlockJSON.apiVersion,
  title: profileBlockJSON.title,
  icon: profileBlockJSON.icon,
  category: profileBlockJSON.category,
  edit: () => {
    return (
      <div {...useBlockProps()}>
        <InnerBlocks 
          allowedBlocks={ [
            'core/image',
            'core/heading',
            'core/paragraph',
            'demo-blocks/first-demo-block'
          ] }
          template={ [
            [ 'core/image', {} ],
            [ 'core/heading', { placeholder: 'Name' } ],
            [ 'core/paragraph', { placeholder: 'Description' } ],
            [ 'demo-blocks/first-demo-block', { content: 'Hello world' }]
          ] }
          templateLock="all"
        />
      </div>
    )
  },
  save: () => {
    return (
      <div {...useBlockProps.save()}>
        <InnerBlocks.Content />
      </div>
    )
  }
}