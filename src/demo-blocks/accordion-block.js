import { 
  InnerBlocks, 
  useBlockProps,
} from '@wordpress/block-editor'

const accordionBlockJSON = require('../../block-configs/accordion-block.json')

export const accordionBlockName = accordionBlockJSON.name

export const accordionBlockData = {
  apiVersion: accordionBlockJSON.apiVersion,
  title: accordionBlockJSON.title,
  icon: accordionBlockJSON.icon,
  category: accordionBlockJSON.category,
  edit({ setAttributes }) {

    const blockProps = useBlockProps()

    // prepare to provide the ID of the parent container to the inner accordion items
    setAttributes({ accordionID: blockProps.id })

    return (
      <div {...blockProps}>
        <InnerBlocks 
          allowedBlocks={[
            accordionBlockJSON.allowedBlocks
          ]}
          template={[
            [ 'demo-blocks/accordion-item-block', {
              itemTitle: '',
              itemContent: ''
            }]
          ]}
          // add a button to add more accordion items
          renderAppender={ InnerBlocks.ButtonBlockerAppender }
        />
      </div>
    )
  },
  save() {
    return (
      <div {...useBlockProps.save({ className: 'ucomm-accordion' })}>
        <InnerBlocks.Content />
      </div>
    )
  }
}