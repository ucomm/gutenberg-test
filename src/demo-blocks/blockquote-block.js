import {
  useBlockProps,
  RichText,
  AlignmentToolbar,
  BlockControls,
  InspectorControls,
  PlainText,
} from '@wordpress/block-editor'

import { PanelBody, SelectControl } from '@wordpress/components'


const blockQuoteJSON = require('../../block-configs/blockquote-block.json')

export const blockQuoteName = blockQuoteJSON.name

export const blockQuoteData = {
  apiVersion: blockQuoteJSON.apiVersion,
  title: blockQuoteJSON.title,
  icon: blockQuoteJSON.icon,
  category: blockQuoteJSON.category,
  attributes: blockQuoteJSON.attributes,
  example: blockQuoteJSON.example,
  edit: (props) => {
    const {
      attributes: {
        quote,
        citation,
        textAlignment,
        backgroundColor,
        borderPosition,
        borderColor,
        textColor
      },
      setAttributes
    } = props

    return (
      <div {...useBlockProps()}>
        <InspectorControls key="setting">
          <PanelBody title="Border Styles">
            <div>
              <fieldset>
                <legend>Border Position</legend>
                <SelectControl 
                  options={[
                    { label: 'Left', value: 'left' },
                    { label: 'Right', value: 'right' }
                  ]} 
                  value={borderPosition}
                  onChange={(newPosition) => setAttributes({ borderPosition: newPosition })}
                />
              </fieldset>
              <fieldset>
                <legend>Border Color</legend>
                <SelectControl
                  options={[
                    { label: 'UConn Blue', value: '#000e2f' },
                    { label: 'Medium Blue', value: '#015999' },
                    { label: 'Cool Grey', value: '#6b7880' },
                    { label: 'Red', value: '#e4002b' }
                  ]}
                  value={backgroundColor}
                  onChange={(newColor) => setAttributes({ borderColor: newColor })}
                />
              </fieldset>
            </div>
          </PanelBody>
          <PanelBody title="Color Styles">
            <div>
              <fieldset>
                <legend>Background Color</legend>
                <SelectControl
                  options={[
                    { label: 'Theme Background', value: 'inherit' },
                    { label: 'White', value: '#ffffff' },
                    { label: 'UConn Blue', value: '#000e2f' },
                    { label: 'Medium Blue', value: '#015999' },
                    { label: 'Cool Grey', value: '#6b7880' },
                    { label: 'Red', value: '#e4002b' }
                  ]}
                  value={backgroundColor}
                  onChange={(newColor) => setAttributes({ backgroundColor: newColor })}
                />
              </fieldset>
            </div>
            <div>
              <fieldset>
                <legend>Text Color</legend>
                <SelectControl
                  options={[
                    { label: 'Theme Background', value: 'inherit' },
                    { label: 'White', value: '#ffffff' },
                    { label: 'UConn Blue', value: '#000e2f' },
                    { label: 'Medium Blue', value: '#015999' },
                    { label: 'Cool Grey', value: '#6b7880' },
                    { label: 'Red', value: '#e4002b' }
                  ]}
                  value={textColor}
                  onChange={(newColor) => setAttributes({ textColor: newColor })}
                />
              </fieldset>
            </div>
          </PanelBody>
        </InspectorControls>
        <BlockControls>
          <AlignmentToolbar
            value={textAlignment}
            onChange={(newAlignment) => setAttributes({ textAlignment: newAlignment })}
          />
        </BlockControls>
        <div 
          className={`blockquote-wrapper`}
          style={{
          // textAlign: textAlignment,
          backgroundColor,
          borderLeft: borderPosition === 'left' ? `4px solid ${borderColor}` : `4px solid transparent`,
          borderRight: borderPosition === 'right' ? `4px solid ${borderColor}` : `4px solid transparent`
        }}>
          <div>
            <blockquote>
              <PlainText 
                value={quote === '' ? '' : quote}
                placeholder="Lorem, ipsum dolor sit amet..."
                onChange={(newQuote) => setAttributes({ quote: newQuote })}
                className={[
                  'uconn-quote-editor',
                  `uconn-quote-align-${textAlignment}`
                ]}
                style={{ color: textColor }}
              />
            </blockquote>
          </div>
          <div>
            <cite>
              <PlainText
                placeholder="An author" 
                value={citation === '' ? '' : citation}
                onChange={(newCitation) => setAttributes({ citation: newCitation })}
                className={[
                  'uconn-cite-editor',
                  `uconn-cite-align-${textAlignment}`
                ]}
                style={{ color: textColor }}
              />
            </cite>
          </div>
        </div>
      </div>
    )
  },
  save: ({ attributes }) => {
    console.log({ attributes });

    const {
      backgroundColor,
      borderColor,
      borderPosition,
      citation,
      quote,
      textAlignment,
      textColor
    } = attributes

    const containerClasses = [`blockquote-wrapper`].join(' ')
    // const quoteClasses = []
    // const citeClasses = []

    return (
      <div {...useBlockProps.save()}>
        <div
          className={containerClasses}
          style={{
            // textAlign: attributes.textAlignment,
            backgroundColor: backgroundColor,
            borderLeft: borderPosition === 'left' ? 
              `4px solid ${borderColor}` : 
              `4px solid transparent`,
            borderRight: borderPosition === 'right' ? 
              `4px solid ${borderColor}` : 
              `4px solid transparent`,
            color: textColor
          }}
        >
          <div>
            <blockquote style={{ color: 'inherit' }}>{quote}</blockquote>
          </div>
          <div>
            <cite
              className={[
                `uconn-cite-align-${textAlignment}`
              ]}
              style={{ color: 'inherit' }}
            >{citation}</cite>
          </div>
        </div>
      </div>
    )
  } 
}