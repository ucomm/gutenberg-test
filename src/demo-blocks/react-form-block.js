import {
  useBlockProps
} from '@wordpress/block-editor'

import { useState } from '@wordpress/element'

const reactFormJSON = require('../../block-configs/react-form-block.json')

import Form from '../components/form'

export const reactFormName = reactFormJSON.name
export const reactFormData = {
  apiVersion: reactFormJSON.apiVersion,
  title: reactFormJSON.title,
  icon: reactFormJSON.icon,
  category: reactFormJSON.category,
  attributes: reactFormJSON.attributes,
  parent: reactFormJSON.parent,
  edit(props) {
    const {
      attributes: {
        containerID,
        formInputText
      },
      setAttributes
    } = props

    const blockProps = useBlockProps()

    console.log({ blockProps });

    setAttributes({ containerID: blockProps.id })

    return (
      <div {...blockProps}>
        hello editor
        <Form 
          word="back" 
        />
      </div>
    )
  },
  save({ attributes }) {
    const { containerID, formInputText } = attributes
    return (
      <div {...useBlockProps.save({
        id: containerID
      })}>
        hello world
      </div>
    )
  }
}