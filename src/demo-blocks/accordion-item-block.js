import { 
  useBlockProps,
  PlainText,
  RichText 
} from '@wordpress/block-editor'

const accordionItemJSON = require('../../block-configs/accordion-item-block.json')

export const accordionItemName = accordionItemJSON.name
export const accordionItemData = {
  apiVersion: accordionItemJSON.apiVersion,
  title: accordionItemJSON.title,
  icon: accordionItemJSON.icon,
  category: accordionItemJSON.category,
  attributes: accordionItemJSON.attributes,
  parent: accordionItemJSON.parent,
  edit(props) {

    const {
      attributes: {
        itemTitle,
        itemContent,
        itemID
      },
      setAttributes,
      context
    } = props

    const blockProps = useBlockProps()

    // the ID of the parent accordion container passed via context between the parent and child
    const parentID = context['demo-blocks/accordionID']

    console.log({ blockProps, parentID });

    setAttributes({ itemID: blockProps.id })

    return (
      <div {...blockProps}>
        <div>
          <button style={{ 
            backgroundColor: 'inherit',
            border: '1px solid',
            padding: '8px 16px',
            width: '100%',
          }}
          >
            <PlainText 
              placeholder="Accordion Item Title"
              value={itemTitle === '' ? '' : itemTitle}
              onChange={(newTitle) => setAttributes({ itemTitle: newTitle })}
              style={{
                backgroundColor: 'inherit',
              }}
            />
          </button>
        </div>
        <div
          style={{ backgroundColor: 'lightgrey' }}
        >
          <RichText 
            tagName="p"
            value={itemContent}
            onChange={(newContent) => setAttributes({ itemContent: newContent })}
            style={{ 
              backgroundColor: 'inherit',
              padding: '0 16px'
            }}
          />
        </div>
      </div>
    )
  },
  save({ attributes }) {

    const { itemTitle, itemContent, itemID } = attributes

    const savedProps = useBlockProps.save({ 
      className: 'ucomm-accordion-wrapper',
      id: itemID 
    })

    console.log({ savedProps });

    return (
      <div {...savedProps}>
        <div 
          className={[
            'accordion-button-wrapper'
          ].join(' ')}
        >
          <button
            aria-expanded="false"
            aria-controls={`accordion-panel-${savedProps.id}`}
          >
            {itemTitle}
          </button>
        </div>
        <div
          id={`accordion-panel-${savedProps.id}`}
          className={[
            'accordion-panel'
          ].join(' ')}
          style={{
            display: 'none'
          }}
        >
          <RichText.Content 
            tagName="p"
            value={itemContent}
          />
        </div>
      </div>
    )
  }
}