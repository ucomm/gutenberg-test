const ucommAccordions = document.querySelectorAll('.ucomm-accordion')

ucommAccordions.forEach(accordion => {
  accordion.addEventListener('click', ({ target }) => {
    
    if (target.localName !== 'button') return
    
    const isExpanded = target.getAttribute('aria-expanded')
    const accordionPanelID = target.getAttribute('aria-controls')
    const accordionPanel = document.querySelector(`#${accordionPanelID}`)

    if (isExpanded === 'false') {
      target.setAttribute('aria-expanded', 'true')
      accordionPanel.style.display = 'block'
      
    } else {
      target.setAttribute('aria-expanded', 'false')
      accordionPanel.style.display = 'none'
    }

  })
})